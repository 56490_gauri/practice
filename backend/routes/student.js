const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

router.post('/addStudent', (request, response) => {
    const { name, standard, division } = request.body
  
    const statement = `
      insert into student
        (name, class, division)
      values 
        ('${name}', '${standard}', '${division}')
    `  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })

  router.post('/findStudentByRollno', (request, response) => {
    const { rollNo } = request.body
    const statement = `
      select * from student where roll_no = '${rollNo}'`
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })

  router.post('/updateStudent', (request, response) => {
    const {rollNo, standard, division } = request.body
    const statement = `
      update student set class ='${standard}',division = '${division}' where roll_no = '${rollNo}'`
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })

  router.post('/deleteStudent', (request, response) => {
    const {rollNo} = request.body
    const statement = `
      delete from student where roll_no = '${rollNo}'`
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })

  router.post('/fetchAllStudentsOfParticularClass', (request, response) => {
    const {standard} = request.body
    const statement = `
      select * from student where class = '${standard}'`
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })

module.exports = router