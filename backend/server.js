const express = require('express')
const cors = require('cors')
const routerStudent = require('./routes/student')

const app = express()

app.use(cors('*'))
app.use(express.json())

app.use('/student',routerStudent)

app.listen(4000,()=>
{console.log('server started on port 4000')}
)